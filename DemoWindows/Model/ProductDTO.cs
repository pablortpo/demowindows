﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWindows.Model
{
    public class ProductDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Trademark { get; set; }
        public double Price { get; set; }
        public DateTime Createdate { get; set; }

        public override string ToString()
        {
            return $"{this.Id.ToString()},{this.Name},{this.Description},{this.Trademark},{this.Price.ToString()},{this.Createdate.ToString("dd/MM/yyyy")}";
        }
    }
}
