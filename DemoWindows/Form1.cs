﻿namespace DemoWindows
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {

        #region Attributes
        private List<Model.ProductDTO> Data;
        #endregion

        #region Builder
        public Form1()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private async void BtnGenerar_Click(object sender, EventArgs e)
        {
            try
            {
                await this.LoadData();
                MessageBox.Show("Se a creado la data");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ocurrió un error: {ex}");
            }
        }

        private async void SfdSaveFile_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog = (System.Windows.Forms.SaveFileDialog)sender;
                await this.SaveData(saveFileDialog.FileName);
                MessageBox.Show("Se a exportado el archivo");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ocurrió un error: {ex}");
            }
        }

        private void BtnLoadFile_Click(object sender, EventArgs e)
        {
            try
            {
                this.sfdSaveFile.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ocurrió un error: {ex}");
            }
        }

        private void BtnLoadFiles_Click(object sender, EventArgs e)
        {
            try
            {
                Views.LoadFile load = new Views.LoadFile();
                load.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ocurrió un error: {ex}");
            }
        }
        #endregion

        #region Private Methods
        private async Task LoadData()
        {
            int process = 0;
            int numberRows = 10;
            int steps = 100 / numberRows;
            this.Data = new List<Model.ProductDTO>();

            for (int i = 0; i < numberRows; i++)
            {
                process = process + steps;
                Data.Add(new Model.ProductDTO()
                {
                    Id = i,
                    Name = $"Nombre {i}",
                    Description = $"Descripcion {i}",
                    Trademark = $"Marca {i}",
                    Price = 100 * i,
                    Createdate = DateTime.Now.AddDays(-i)
                });

                this.pgbProgress.Value = process;
            }
        }

        private async Task SaveData(string path)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Id,Nombre,Descripcion,Marca,Precio,Fecha ingreso" + System.Environment.NewLine);
            foreach (Model.ProductDTO item in this.Data)
            {
                stringBuilder.AppendLine(item.ToString());
            }

            System.IO.File.WriteAllText(path, stringBuilder.ToString());
        }
        #endregion
    }
}
