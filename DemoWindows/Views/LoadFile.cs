﻿namespace DemoWindows.Views
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using DemoWindows.Model;

    public partial class LoadFile : Form
    {
        #region Attributes
        private string pathFile;
        private List<Model.ProductDTO> productsList;
        #endregion

        #region Properties
        public List<ProductDTO> ProductsList { get => productsList; set => productsList = value; }
        #endregion

        #region Builder
        public LoadFile()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private void BtnAddFile_Click(object sender, EventArgs e)
        {

            try
            {
                //this.ofdSearchFile.ShowDialog();
                //Se aplica un using para el diaglo para que libere el archivo una vez que termine el using y así poderlo leer
                using (OpenFileDialog openFileDialog1 = new OpenFileDialog())
                {
                    openFileDialog1.Title = "Buscar Archivo";
                    //openFileDialog1.Filter = "cvs *.cvs;*.CVS;";
                    openFileDialog1.Multiselect = false;

                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        this.pathFile = openFileDialog1.FileName;
                    }
                }

                this.textBox1.Text = this.pathFile;
                this.btnLoad.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ocurrió un error: {ex}");
            }
        }

        private async void BtnLoad_Click(object sender, EventArgs e)
        {
            try
            {
                await this.LoadGridView();
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ocurrió un error: {ex}");
            }
        }
        #endregion

        #region Private Methods
        private async Task LoadGridView()
        {
            this.ProductsList = new List<Model.ProductDTO>();
            List<string> datosString = System.IO.File.ReadLines(this.pathFile).ToList();
            bool isFirstRow = true;

            string message = await this.StructureFileValidation(datosString);
            if (string.IsNullOrEmpty(message))
            {
                foreach (var item in datosString)
                {
                    if (isFirstRow)
                    {
                        isFirstRow = false;
                        continue;
                    }
                    else
                    {
                        string[] values = item.Split(',');
                        Model.ProductDTO product = new ProductDTO()
                        {
                            Id = Convert.ToInt32(values[0]),
                            Name = values[1],
                            Description = values[2],
                            Trademark = values[3],
                            Price = Convert.ToDouble(values[4]),
                            Createdate = Convert.ToDateTime(values[5])
                        };

                        this.ProductsList.Add(product);
                    }
                }

                this.grvData.DataSource = this.productsList;
            }
            else
            {
                MessageBox.Show(message, "Alerta");
                MessageBox.Show("El archivo cargado no continiene la estructura para esta importación", "Error");
            }

        }

        private async Task<string> StructureFileValidation(List<string> datosString)
        {
            List<string> result = new List<string>();
            string firstLine = datosString.FirstOrDefault();

            List<string> columns = firstLine.Split(',').ToList();
            if (columns.Count != 6)
            {
                result.Add("El archivo debe tener 6 columnas");
            }
            else
            {
                if (!columns[0].ToLower().Equals("id"))
                {
                    result.Add($"La columna [{0 + 1}] debe ser [Id]");
                }

                if (!columns[1].ToLower().Equals("nombre"))
                {
                    result.Add($"La columna [{1 + 1}] debe ser [Nombre]");
                }

                if (!columns[2].ToLower().Equals("descripcion"))
                {
                    result.Add($"La columna [{2 + 1}] debe ser [Descripcion]");
                }

                if (!columns[3].ToLower().Equals("marca"))
                {
                    result.Add($"La columna [{3 + 1}] debe ser [Marca]");
                }

                if (!columns[4].ToLower().Equals("precio"))
                {
                    result.Add($"La columna [{4 + 1}] debe ser [Precio]");
                }

                if (!columns[5].ToLower().Equals("fecha ingreso"))
                {
                    result.Add($"La columna [{5 + 1}] debe ser [Fecha ingreso]");
                }
            }
            return string.Join(Environment.NewLine, result);
        }
        #endregion
    }
}
